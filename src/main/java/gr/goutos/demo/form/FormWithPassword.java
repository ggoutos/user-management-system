package gr.goutos.demo.form;

public interface FormWithPassword {

    String password = null;
    String passwordConfirm = null;

    String getPassword();
    String getPasswordConfirm();


}
