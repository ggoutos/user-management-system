package gr.goutos.demo.form;

import gr.goutos.demo.utils.GlobalAttributes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UpdateUserForm {

    private long id;

    @NotBlank(message = GlobalAttributes.NAME_EMPTY_MESSAGE)
    private String name;

    @NotBlank(message = GlobalAttributes.SURNAME_EMPTY_MESSAGE)
    private String surname;

    @Pattern(regexp = GlobalAttributes.MAIL_PATTERN, message = GlobalAttributes.MAIL_PATTERN_MESSAGE)
    private String email;

    private String telephone;

    private String company;

    public UpdateUserForm(long id, String name, String surname, String email, String telephone, String company) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
        this.company = company;
    }

    public UpdateUserForm() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
