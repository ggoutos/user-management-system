package gr.goutos.demo.form;

import gr.goutos.demo.utils.GlobalAttributes;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegisterUserForm implements FormWithPassword{

    private long id;

    @NotBlank(message = GlobalAttributes.NAME_EMPTY_MESSAGE)
    private String name;

    @NotBlank(message = GlobalAttributes.SURNAME_EMPTY_MESSAGE)
    private String surname;

    @Pattern(regexp = GlobalAttributes.MAIL_PATTERN, message = GlobalAttributes.MAIL_PATTERN_MESSAGE)
    private String email;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    @Pattern(regexp = GlobalAttributes.PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    private String password;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    @Pattern(regexp = GlobalAttributes.PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    private String passwordConfirm;

    private String telephone;

    private String company;

    public RegisterUserForm(long id, String name, String surname, String email, String telephone, String company) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.telephone = telephone;
        this.company = company;
    }

    public RegisterUserForm() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
