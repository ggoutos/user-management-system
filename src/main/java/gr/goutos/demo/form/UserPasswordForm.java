package gr.goutos.demo.form;

import gr.goutos.demo.utils.GlobalAttributes;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserPasswordForm implements FormWithPassword{

    private long id;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    @Pattern(regexp = GlobalAttributes.PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    private String oldPassword;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    @Pattern(regexp = GlobalAttributes.PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    private String password;

    @Size(min = GlobalAttributes.PASSWORD_SIZE, message = GlobalAttributes.PASSWORD_SIZE_MESSAGE)
    @Pattern(regexp = GlobalAttributes.PASSWORD_PATTERN, message = GlobalAttributes.PASSWORD_PATTERN_MESSAGE)
    private String passwordConfirm;

    public UserPasswordForm(long id, String oldPassword, String password, String passwordConfirm) {
        this.id = id;
        this.oldPassword = oldPassword;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public UserPasswordForm() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
