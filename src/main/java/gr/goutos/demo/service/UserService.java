package gr.goutos.demo.service;

import gr.goutos.demo.domain.User;
import gr.goutos.demo.form.RegisterUserForm;
import gr.goutos.demo.form.UpdateUserForm;
import gr.goutos.demo.form.UserPasswordForm;

public interface UserService {

    User getUserById(long id);

    User getUserByEmail(String email);

    void create(RegisterUserForm registerUserForm);

    void update(UpdateUserForm updateUserForm);

    void updatePassword(UserPasswordForm userPasswordForm);

    void delete(long id);

}
