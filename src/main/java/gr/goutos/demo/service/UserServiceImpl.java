package gr.goutos.demo.service;


import gr.goutos.demo.domain.User;
import gr.goutos.demo.exception.EmailExistsException;
import gr.goutos.demo.exception.IncorrectPasswordException;
import gr.goutos.demo.form.RegisterUserForm;
import gr.goutos.demo.form.UpdateUserForm;
import gr.goutos.demo.form.UserPasswordForm;
import gr.goutos.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User getUserById(long id) {
        User user = userRepository.findById(id);
        return user;
    }

    public User getUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    public void create(RegisterUserForm registerUserForm) {
        User user = new User(registerUserForm.getId(), registerUserForm.getName(), registerUserForm.getSurname(), registerUserForm.getEmail(),
                passwordEncoder.encode(registerUserForm.getPassword()), registerUserForm.getTelephone(), registerUserForm.getCompany());

        User userWithTheSameEmail = userRepository.findByEmail(registerUserForm.getEmail());

        if (userWithTheSameEmail != null) {
            throw (new EmailExistsException());
        } else {
            userRepository.save(user);
        }
    }

    @Override
    public void update(UpdateUserForm form) {
        User updatingUser = new User(form.getId(), form.getName(), form.getSurname(),
                form.getEmail(), form.getTelephone(), form.getCompany());

        User oldUser = userRepository.findById(form.getId());
        updatingUser.setPassword(oldUser.getPassword());

        //check if new email is used by another user
        User existingUser = userRepository.findByEmail(form.getEmail());
        if (existingUser != null && existingUser.getEmail().equals(form.getEmail()) && existingUser.getId() != form.getId()) {
            throw new EmailExistsException();
        }

        userRepository.save(updatingUser);
    }

    @Override
    public void updatePassword(UserPasswordForm form) {
        User user = userRepository.findById(form.getId());

        if (!passwordEncoder.matches(form.getOldPassword(), user.getPassword())) {
            throw new IncorrectPasswordException();
        }

        user.setPassword(passwordEncoder.encode(form.getPassword()));
        userRepository.save(user);
    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
    }

}
