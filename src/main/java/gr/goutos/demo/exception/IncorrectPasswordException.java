package gr.goutos.demo.exception;

import gr.goutos.demo.utils.GlobalAttributes;

public class IncorrectPasswordException extends RuntimeException {
    public IncorrectPasswordException() {
        super(GlobalAttributes.INCORRECT_PASSWORD);
    }

}
