package gr.goutos.demo.exception;

import gr.goutos.demo.utils.GlobalAttributes;

public class EmailExistsException extends RuntimeException {
    public EmailExistsException() {
        super(GlobalAttributes.USER_MAIL_EXISTS);
    }

}
