package gr.goutos.demo.security;

import gr.goutos.demo.domain.User;
import gr.goutos.demo.repository.UserRepository;
import gr.goutos.demo.utils.GlobalAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    UserRepository userRepository;

    private static final String ADMIN_HOME_PAGE_URL = "/";
    private static final String DEFAULT_ROLE = "USER";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        HttpSession session = request.getSession();

        User user = userRepository.findByEmail(authentication.getName());

        session.setAttribute("id", user.getId());
        session.setAttribute("name", user.getName());
        session.setAttribute("surname", user.getSurname());
        session.setAttribute("role", DEFAULT_ROLE);

        response.addCookie(generateTimestampCookie());

        redirectStrategy.sendRedirect(request, response, ADMIN_HOME_PAGE_URL);
    }

    private Cookie generateTimestampCookie() {
        return new Cookie(GlobalAttributes.TIMESTAMP_COOKIE_NAME, String.valueOf(System.currentTimeMillis()));
    }


}