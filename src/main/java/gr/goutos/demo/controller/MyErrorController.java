package gr.goutos.demo.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {

    @GetMapping(value = "/error")
    public String error(Model model, HttpServletRequest request) {
        HttpStatus status = HttpStatus.valueOf((Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
        String errorMsg = status.getReasonPhrase();

        model.addAttribute("status", status.value());
        model.addAttribute("message", errorMsg);

        return "error";
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}
