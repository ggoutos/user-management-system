package gr.goutos.demo.controller;

import gr.goutos.demo.domain.User;
import gr.goutos.demo.exception.EmailExistsException;
import gr.goutos.demo.exception.IncorrectPasswordException;
import gr.goutos.demo.form.RegisterUserForm;
import gr.goutos.demo.form.UpdateUserForm;
import gr.goutos.demo.form.UserPasswordForm;
import gr.goutos.demo.service.RecaptchaService;
import gr.goutos.demo.service.UserService;
import gr.goutos.demo.utils.GlobalAttributes;
import gr.goutos.demo.validator.PasswordValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class UserController {

    private static final String REGISTER_USER_FORM = "registerUserForm";

    @Autowired
    UserService userService;

    @Autowired
    PasswordValidator passwordValidator;

    @Autowired
    RecaptchaService recaptchaService;

    @InitBinder({"registerUserForm", "userPasswordForm"})
    protected void initRegisterBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(passwordValidator);
    }

    @GetMapping("/users/register")
    public String registerGET(Model model) {
        if (!model.containsAttribute(REGISTER_USER_FORM)) {
            RegisterUserForm registerUserForm = new RegisterUserForm();
            model.addAttribute(REGISTER_USER_FORM, registerUserForm);
        }
        return "user-register";
    }

    @PostMapping(value = "/users/register")
    public String registerPOST(Model model, @Valid RegisterUserForm registerUserForm,
                               @RequestParam(name = "g-recaptcha-response") String recaptchaResponse,
                               HttpServletRequest request,
                               BindingResult bindingResult) {

        String ip = request.getRemoteAddr();
        String captchaVerifyMessage = recaptchaService.verifyRecaptcha(ip, recaptchaResponse);

        if (StringUtils.isNotEmpty(captchaVerifyMessage)) {
            model.addAttribute("incorrectCaptcha", GlobalAttributes.CAPTCHA_ERROR);
            return "user-register";
        }

        if (bindingResult.hasErrors()) {
            return "user-register";
        }
        try {
            userService.create(registerUserForm);
        } catch (EmailExistsException e) {
            model.addAttribute("errorInEmail", e.getMessage());
            return "user-register";
        }
        return "redirect:/";
    }

    @GetMapping(value = "/users/update/{id}")
    public String updateGET(@PathVariable(name = "id") long id, Model model) {
        User user = userService.getUserById(id);
        UpdateUserForm form = new UpdateUserForm(user.getId(), user.getName(), user.getSurname(), user.getEmail(), user.getTelephone(), user.getCompany());
        model.addAttribute("userUpdateForm", form);
        return "user-update";
    }

    @RequestMapping(value = "/users/update", method = RequestMethod.POST)
    public String updatePOST(Model model, @Valid @ModelAttribute("userUpdateForm") UpdateUserForm form,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user-update";
        }
        try {
            userService.update(form);
        } catch (EmailExistsException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "user-update";
        }
        return "redirect:/";
    }

    @GetMapping(value = "/users/password/{id}")
    public String passwordGET(@PathVariable(name = "id") long id, Model model) {
        UserPasswordForm form = new UserPasswordForm();
        form.setId(id);
        model.addAttribute("userPasswordForm", form);
        return "user-password";
    }

    @RequestMapping(value = "/users/password", method = RequestMethod.POST)
    public String passwordPOST(Model model, @Valid @ModelAttribute("userPasswordForm") UserPasswordForm form,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user-password";
        }
        try {
            userService.updatePassword(form);
        } catch (IncorrectPasswordException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "user-password";
        }
        return "redirect:/";
    }

}
