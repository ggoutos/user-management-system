package gr.goutos.demo.controller;

import gr.goutos.demo.domain.User;
import gr.goutos.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class IndexController {

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String index(Model model, HttpSession session) {
        User user = userService.getUserById((Long) session.getAttribute("id"));
        model.addAttribute("user", user);
        return "index";
    }


}
