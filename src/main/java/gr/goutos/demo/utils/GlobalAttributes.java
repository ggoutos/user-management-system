package gr.goutos.demo.utils;

public class GlobalAttributes {
    public static final String TIMESTAMP_COOKIE_NAME = "TIMESTAMP";

    /* patterns */
    public static final String MAIL_PATTERN = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$";
    public static final String PASSWORD_PATTERN = "^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).*$";
    public static final int PASSWORD_SIZE = 8;

    /* messages */
    public static final String NAME_EMPTY_MESSAGE = "Name is required.";
    public static final String SURNAME_EMPTY_MESSAGE = "Surname is required.";
    public static final String MAIL_PATTERN_MESSAGE = "Please use a valid email";
    public static final String PASSWORD_SIZE_MESSAGE = "Password must be at least 8 characters long.";
    public static final String PASSWORD_PATTERN_MESSAGE = "Password must contain at least: 1 capital letter, 1 lowercase letter, 1 number and 1 special character.";
    public static final String PASSWORD_CONFIRM_MESSAGE = "Must be equal to password.";
    public static final String USER_MAIL_EXISTS = "User with this Email already exists";
    public static final String INCORRECT_PASSWORD = "Old password is incorrect. Please try again.";
    public static final String CAPTCHA_ERROR = "Are you a robot?";
}
