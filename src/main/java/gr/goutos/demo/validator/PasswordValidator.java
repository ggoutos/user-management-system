package gr.goutos.demo.validator;

import gr.goutos.demo.form.FormWithPassword;
import gr.goutos.demo.utils.GlobalAttributes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return FormWithPassword.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        FormWithPassword form = (FormWithPassword) target;
        String password = form.getPassword();
        String passwordConfirm = form.getPasswordConfirm();

        if (!password.equals(passwordConfirm)) {
            errors.rejectValue("passwordConfirm", "passwordConfirm", GlobalAttributes.PASSWORD_CONFIRM_MESSAGE);
        }
    }
}
