package gr.goutos.demo.repository;

import gr.goutos.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    void deleteById(Long id);

    User findByEmail(String email);

    User findById(long id);

    User save(User user);

}
