jQuery(function () {
    $('#registerUserForm').validate({
        onkeyup: false,
        onfocusout: false,
        rules: {
            name: {
                required: true
            },
            surname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            },
            passwordConfirm: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
        },
        messages: {
            email: {
                email: "Please enter a valid email"
            },
            password: {
                minlength: "Length must be more than 8 characters"
            },
            passwordConfirm: {
                minlength: "Length must be more than 8 characters",
                equalTo: "Must be equal to password"
            }
        }
    });
});