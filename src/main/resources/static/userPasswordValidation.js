jQuery(function () {
    $('#userPasswordForm').validate({
        onkeyup: false,
        onfocusout: false,
        rules: {
            oldPassword: {
                required: true,
                minlength: 8
            },
            password: {
                required: true,
                minlength: 8
            },
            passwordConfirm: {
                required: true,
                minlength: 8,
                equalTo: "#password"
            }
        },
        messages: {
            oldPassword: {
                minlength: "Length must be more than 8 characters"
            },
            password: {
                minlength: "Length must be more than 8 characters"
            },
            passwordConfirm: {
                minlength: "Length must be more than 8 characters",
                equalTo: "Must be equal to new password"
            }
        }
    });
});