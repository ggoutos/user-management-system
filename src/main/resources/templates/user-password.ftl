<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <#include "partials/head.ftl">
    <title>User - Update</title>
</head>
<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Change your password</h1>

        <#import "/spring.ftl" as spring />
        <form id="userPasswordForm" action="/users/password" name="userPasswordForm" method="post">

            <input type="hidden" class="form-control" width="270" name="id"
                   value="${userPasswordForm.id}"/>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword">Old Password</label>
                    <@spring.bind "userPasswordForm.oldPassword"/>
                    <input type="password" class="form-control" id="oldPassword" placeholder="Old Password" name="oldPassword">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword">New Password</label>
                    <@spring.bind "userPasswordForm.password"/>
                    <input type="password" class="form-control" id="password" placeholder="New Password" name="password">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPasswordConfirm">Password Confirmation</label>
                    <@spring.bind "userPasswordForm.passwordConfirm"/>
                    <input type="password" class="form-control" id="passwordConfirm" placeholder="Password Confirmation" name="passwordConfirm">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
            <p class="errorMsg" style="color: red">
                <strong>${errorMessage!''}</strong>
            </p>
        </form>

    </div>
</main>

<#include "partials/scripts.ftl">

</body>
</html>