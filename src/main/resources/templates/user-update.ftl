<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <#include "partials/head.ftl">
    <title>User - Update</title>
</head>
<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Update</h1>

        <#import "/spring.ftl" as spring />
        <form id="userUpdateForm" action="/users/update" name="userUpdateForm" method="post">

            <input type="hidden" class="form-control" width="270" name="id"
                   value="${userUpdateForm.id}"/>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Name</label>
                    <@spring.bind "userUpdateForm.name"/>
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="${userUpdateForm.name!''}">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputSurname">Surname</label>
                    <@spring.bind "userUpdateForm.surname"/>
                    <input type="text" class="form-control" value="${userUpdateForm.surname!''}" id="inputSurname" placeholder="Surname" name="surname">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputTelephone">Telephone</label>
                    <@spring.bind "userUpdateForm.telephone"/>
                    <input type="text" class="form-control" id="inputTelephone" placeholder="Telephone" name="telephone" value="${userUpdateForm.telephone!''}">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCompany">Company</label>
                    <@spring.bind "userUpdateForm.company"/>
                    <input type="text" class="form-control" value="${userUpdateForm.company!''}" id="inputCompany" placeholder="Company" name="company">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <@spring.bind "userUpdateForm.email"/>
                    <input type="text" class="form-control" value="${userUpdateForm.email!''}" id="inputEmail" placeholder="Email" name="email">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                    <div>
                        <#if errorInEmail??>
                            <div class="alert alert-danger" role="alert"><span>${errorInEmail}</span></div>
                        </#if>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Save</button>
            <p class="errorMsg" style="color: red">
                <strong>${errorMessage!''}</strong>
            </p>
        </form>

    </div>
</main>

<#include "partials/scripts.ftl">

</body>
</html>