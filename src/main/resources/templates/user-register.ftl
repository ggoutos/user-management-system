<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>User Registration</title>
</head>

<body>


<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Registration</h1>


        <#import "/spring.ftl" as spring />
        <form id="registerUserForm" onsubmit="this.validate()" action="/users/register" name="registerUserForm" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Name</label>
                    <@spring.bind "registerUserForm.name"/>
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="${registerUserForm.name!''}">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputSurname">Surname</label>
                    <@spring.bind "registerUserForm.surname"/>
                    <input type="text" class="form-control" value="${registerUserForm.surname!''}" id="inputSurname" placeholder="Surname" name="surname">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputTelephone">Telephone</label>
                    <@spring.bind "registerUserForm.telephone"/>
                    <input type="text" class="form-control" id="inputTelephone" placeholder="Telephone" name="telephone" value="${registerUserForm.telephone!''}">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputCompany">Company</label>
                    <@spring.bind "registerUserForm.company"/>
                    <input type="text" class="form-control" value="${registerUserForm.company!''}" id="inputCompany" placeholder="Company" name="company">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <@spring.bind "registerUserForm.email"/>
                    <input type="text" class="form-control" value="${registerUserForm.email!''}" id="inputEmail" placeholder="Email" name="email">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                    <div>
                    <#if errorInEmail??>
                        <div class="alert alert-danger" role="alert"><span>${errorInEmail}</span></div>
                    </#if>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputPassword">Password</label>
                    <@spring.bind "registerUserForm.password"/>
                    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPasswordConfirm">Password Confirmation</label>
                    <@spring.bind "registerUserForm.passwordConfirm"/>
                    <input type="password" class="form-control" id="inputPasswordConfirm" placeholder="Password Confirmation" name="passwordConfirm">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>
            <div class="g-recaptcha" data-sitekey="6LeqO4sUAAAAAHtocmTFl-KaNY4yjA-Yb3M8Yzxw"></div>
            <#if incorrectCaptcha??>
                <div class="alert alert-danger" role="alert"><span>${incorrectCaptcha}</span></div>
            </#if>
            <button type="submit" class="btn btn-primary">Register</button>
        </form>

    </div>
</main>

<#include "partials/scripts.ftl">
<script src='https://www.google.com/recaptcha/api.js'></script>


</body>
</html>
