<!DOCTYPE html>
<html lang="en">

<head>
    <#include "partials/head.ftl">
    <title>Error Page</title>
</head>

<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="/">User Management System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Error Page</h1>
        <h2>Error: ${status!'Default error'} - ${message!'message'}</h2>
    </div>

    <#include "partials/scripts.ftl">

</body>
</html>