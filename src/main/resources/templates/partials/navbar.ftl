    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="/">User Management System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Users</a>
                        <ul class="dropdown-menu" style="background-color: #343a40">
                            <li><a class="nav-link" style="line-height: 0.75" href="/users/register">Create</a></li>
                            <li><a class="nav-link" style="line-height: 0.75" href="/users/update/${Session.id}">Edit</a></li>
                            <li><a class="nav-link" style="line-height: 0.75" href="/users/password/${Session.id}">Password</a></li>
                        </ul>
                    </li>
            </ul>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">

                <li class="dropdown-submenu">
                    <a tabindex="-1" href="#">More options</a>
                    <ul class="dropdown-menu">
                        ...
                    </ul>
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link">${Session.name!'-'} ${Session.surname!'-'} </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
