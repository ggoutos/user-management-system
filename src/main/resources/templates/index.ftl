<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <#include "partials/head.ftl">
    <title>User Management System</title>
</head>
<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">My Account</h1>


        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" placeholder="Name" value="${user.name!''}"
                       name="name" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="inputSurname">Surname</label>
                <input type="text" class="form-control" id="inputSurname" placeholder="Surname"
                       value="${user.surname!''}" name="surname" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputTelephone">Telephone</label>
                <input type="text" class="form-control" id="inputTelephone" placeholder="Telephone"
                       value="${user.telephone!''}" name="telephone" readonly>
            </div>
            <div class="form-group col-md-6">
                <label for="inputCompany">Company</label>
                <input type="text" class="form-control" id="inputCompany" placeholder="Company"
                       value="${user.company!''}" name="company" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail">Email</label>
                <input type="text" class="form-control" id="inputEmail" placeholder="Email"
                       value="${user.email!''}" name="email" readonly>
            </div>
        </div>

    </div>
</main>

<#include "partials/scripts.ftl">

</body>
</html>