<!doctype html>
<html lang="en">
<head>
  <#include "partials/head.ftl">
  <title>User Management - Login</title>
</head>

  <body class="text-center">
    <form class="form-signin" name='login' action="/login" method='POST'>
      <img class="mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72">
      <input type="text" name="username" id="username" class="form-control" placeholder="Email address" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password"  name="password" id="password" class="form-control" placeholder="Password" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
    </form>

    <#include "partials/scripts.ftl">
  </body>
</html>
